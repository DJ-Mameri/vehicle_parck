<?php

namespace Domain;

class VehicleFactory
{
    public static function create($marque, $matriculation)
    {
        return new Vehicle($marque, $matriculation);
    }
}
