<?php

namespace Domain;

class Vehicle
{
    private $id;
    private $marque;
    private $matriculation;
    private $location;

    public function __construct($marque, $matriculation)
    {
        $this->marque = $marque;
        $this->matriculation = $matriculation;
        $this->location = array();
    }

    /**
     * Get the value of id
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get the value of marque
     */
    public function getMarque(): ?string
    {
        return $this->marque;
    }

    /**
     * Set the value of marque
     *
     * @return  self
     */
    public function setMarque($marque): self
    {
        $this->marque = $marque;

        return $this;
    }

    /**
     * Get the value of matriculation
     */
    public function getMatriculation(): ?string
    {
        return $this->matriculation;
    }

    /**
     * Set the value of matriculation
     *
     * @return  self
     */
    public function setMatriculation($matriculation): self
    {
        $this->matriculation = $matriculation;

        return $this;
    }

    /**
     * @return string
     */

    public function getName(): ?string
    {
        return 'Vehicule ' . $this->marque . 'Matriculation' . $this->matriculation;
    }

    /**
     * Get the value of location
     */
    public function getLocation(): array
    {
        return $this->location;
    }

    /**
     * Set the value of location
     *
     * @return  self
     */
    public function parkOn($location)
    {
        $this->location = $location;

        return $this;
    }

    public function verifyThisLocation($location)
    {
        return ($this->location == $location);
    }
}
