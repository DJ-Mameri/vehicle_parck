<?php

namespace Domain;

class Location
{
    private $pos_lat;
    private $pos_long;

    public function __construct($pos_lat, $pos_long)
    {
        $this->pos_lat = $pos_lat;
        $this->pos_long = $pos_long;
    }

    /**
     * Get the value of pos_lat
     */
    public function getPos_lat()
    {
        return $this->pos_lat;
    }

    /**
     * Set the value of pos_lat
     *
     * @return  self
     */
    public function setPos_lat($pos_lat)
    {
        $this->pos_lat = $pos_lat;

        return $this;
    }

    /**
     * Get the value of pos_long
     */
    public function getPos_long()
    {
        return $this->pos_long;
    }

    /**
     * Set the value of pos_long
     *
     * @return  self
     */
    public function setPos_long($pos_long)
    {
        $this->pos_long = $pos_long;

        return $this;
    }
}
