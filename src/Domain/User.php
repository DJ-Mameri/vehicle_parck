<?php

namespace Domain;

use Domain\Fleet;

class User
{
    private $id;
    private $name;
    private $email;
    private $fleets;

    public function __construct()
    {
        $this->fleets = array();
    }

    /**
     * Get the value of id
     */
    public function getId(): ?int
    {
        return $this->id;
    }

  
    /**
     * Get the value of name
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */
    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of email
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */
    public function setEmail($email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of fleets
     */
    public function getFleets()
    {
        return $this->fleets;
    }

    /**
     * Set the value of fleet
     *
     * @return  self
     */
    public function addFleet(Fleet $fleet)
    {
        // Verify if fleets contains $fleet
        $this->fleets[] = $fleet;
        $fleet->setUser($this);

        return $this;
    }
}
