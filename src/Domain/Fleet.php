<?php

namespace Domain;

use Domain\Vehicle;

class Fleet
{
    private $id;
    private $vehicles;
    private $user;

    public function __construct($user_id)
    {
        $this->user = $user_id;
        $this->vehicles = array();
    }
    

    /**
     * Get the value of id
     */
    public function getId(): ?int
    {
        return $this->id;
    }


    /**
     * Get the value of vehicles
     */
    public function getVehicles(): array
    {
        return $this->vehicles;
    }

    /**
     * Add vehicle to the global system and this fleet
     *
     *
     */
    public function addVehicle(Vehicle $vehicle)
    {
        $inp = file_get_contents('././fake_data.json');
        $tempArray = json_decode($inp, true);
    
        if (!$this->existVehicle($vehicle->getMatriculation())) {
            // Add this vehicles to the global system
            array_push(
                $tempArray['vehicles'],
                array(
                    "id" => count($tempArray['vehicles']) + 1,
                    "marque" => $vehicle->getMarque(),
                    "matriculation" => $vehicle->getMatriculation()
                )
            );
            // Add this vehicle to this fleet
            array_push(
                $tempArray['fleet'],
                array(
                "id" => count($tempArray['fleet']) + 1,
                "vehicle_id" => count($tempArray['vehicles']),
                "user_id" => (int)$this->user
            )
            );

            $jsonData = json_encode($tempArray);
            file_put_contents('././fake_data.json', $jsonData);
        }
    }

    /**
     * Verify if this vehicle is in the global system and in the this fleet
     * @param string $matriculation
     * @return bool
     */

    public function existVehicle($matriculation): bool
    {
        $inp = file_get_contents('././fake_data.json');
        $tempArray = json_decode($inp, true);
        $vehicles = $tempArray['vehicles'];
        $fleets = $tempArray['fleet'];

        $vehicleSystemFiltered = array_filter($vehicles, function($vehicles) use ($matriculation) {
            return ($vehicles['matriculation'] == $matriculation);
        });

        // Test if this vehicle is in the global system
        if (count($vehicleSystemFiltered) > 0){
            $vehicleFleetFiltered = array_filter($fleets, function($fleets) use ($vehicleSystemFiltered) {
                return (array_values($vehicleSystemFiltered)[0]['id'] == $fleets['vehicle_id']);
            });

            // Test if this vehicle is in this fleet
            return (count($vehicleFleetFiltered) > 0);
        }

        return false;
    }

 
    /**
     * Get the value of user
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * Set the value of user
     *
     * @return  self
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get the value of name
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */
    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }
}
