<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Domain\Fleet;
use Domain\Location;
use Domain\VehicleFactory;

/**
 * Defines application features from the specific context.
 */
class ParkVehicleContext implements Context
{
    
    private $fleet;
    private $vehicle;
    private $location;
    
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct($fleet_user_id, $vehicle_marque, $vehicle_matriculation)
    {
        $this->fleet = new Fleet($fleet_user_id);
        $this->vehicle = VehicleFactory::create($vehicle_marque, $vehicle_matriculation);
    }


    /**
     * @Given a vehicle marque :marque and matriculation :matriculation
     */
    public function aVehicle($marque, $matriculation)
    {
        $this->vehicle = VehicleFactory::create($marque, $matriculation);
    }

    /**
     * @Given a location :arg1 and :arg2
     */
    public function aLocationAnd($arg1, $arg2)
    {
        $this->location = new Location($arg1, $arg2);
    }

    /**
     * @When I park my vehicle at this location
     */
    public function iParkMyVehicleAtThisLocation()
    {
        $this->vehicle->parkOn($this->location);
    }

    /**
     * @Then the known location of my vehicle should verify this location
     */
    public function theKnownLocationOfMyVehicleShouldVerifyThisLocation()
    {
        $this->vehicle->verifyThisLocation($this->location);
    }

    /**
     * @Given my vehicle has been parked into this location
     */
    public function myVehicleHasBeenParkedIntoThisLocation()
    {
        $this->vehicle->parkOn($this->location);
    }

    /**
     * @When I try to park my vehicle at this location
     */
    public function iTryToParkMyVehicleAtThisLocation()
    {
        try {
            $this->vehicle->parkOn($this->location);
        } catch (\Throwable $th) {
            throw $th;
        }
        
    }

    /**
     * @Then I should be informed that my vehicle is already parked at this location
     */
    public function iShouldBeInformedThatMyVehicleIsAlreadyParkedAtThisLocation()
    {
       if($this->vehicle->verifyThisLocation($this->location)){
            echo "My vehicle is already parked at this location";
       }else {
            throw new Exception("My vehicle is not parked at this location");
       }
    }
}
