<?php

use Domain\Fleet;
use Domain\Location;
use Domain\VehicleFactory;
use Behat\Behat\Context\Context;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class RegisterVehicleContext implements Context
{
    
    private $fleet1;
    private $fleet2;
    private $vehicle;
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {

    }

      /**
     * @Given my fleet of user :user_id
     */
    public function myFleetOfUser($user_id)
    {
        $this->fleet1 = new Fleet($user_id);
    }

     /**
     * @Given a vehicle with marque :marque and matriculation :matriculation
     */
    public function aVehicleWithMarqueAndMatriculation($marque, $matriculation)
    {
        $this->vehicle = VehicleFactory::create($marque, $matriculation);
    }

     /**
     * @When I register this vehicle into my fleet
     */
    public function iRegisterThisVehicleIntoMyFleet()
    {
        $this->fleet1->addVehicle($this->vehicle);
    }

    /**
     * @Then this vehicle should be part of my vehicle fleet
     */
    public function thisVehicleShouldBePartOfMyVehicleFleet()
    {

        if (!$this->fleet1->existVehicle($this->vehicle->getMatriculation())) {
            throw new Exception('This vehicle is not part of your vehicle fleet');
        }
        
    }

    /**
     * @Given I have registered this vehicle into my fleet
     */
    public function iHaveRegisteredThisVehicleIntoMyFleet()
    {
        if($this->vehicle != null ) 
            $this->fleet1->addVehicle($this->vehicle);
    }

    /**
     * @When I try to register this vehicle into my fleet
     */
    public function iTryToRegisterThisVehicleIntoMyFleet()
    {
        $this->fleet1->addVehicle($this->vehicle);
    }

    /**
     * @Then I should be informed this vehicle has already been registered into my fleet
     */
    public function iShouldBeInformedThisVehicleHasAlreadyBeenRegisteredIntoMyFleet()
    {
        if ($this->fleet1->existVehicle($this->vehicle->getMatriculation())) {
            echo('This vehicle has already been registered into my fleet');
        }
        
    }

    /**
     * @Given the fleet of another user :user_id
     */
    public function theFleetOfAnotherUser($user_id)
    {
        $this->fleet2 = new Fleet($user_id);
    }

  
    /**
     * @Given this vehicle has been registered into the other user's fleet
     */
    public function thisVehicleHasBeenRegisteredIntoTheOtherUsersFleet()
    {
        $this->fleet2->addVehicle($this->vehicle);
    }

}
