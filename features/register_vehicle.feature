Feature: Register a vehicle

    In order to follow many vehicles with my application
    As an application user
    I should be able to register my vehicle

    @critical
    Scenario: I can register a vehicle when I fill the marque and matriculation
        Given my fleet of user "1"
        And a vehicle with marque "peugeot" and matriculation "AL-XX-YYY"
        When I register this vehicle into my fleet
        Then this vehicle should be part of my vehicle fleet
    Scenario: I can't register same vehicle twice
        Given my fleet of user "1"
        And a vehicle with marque "peugeot" and matriculation "AL-ZZ-YYY"
        And I have registered this vehicle into my fleet
        When I try to register this vehicle into my fleet
        Then I should be informed this vehicle has already been registered into my fleet
    Scenario: Same vehicle can belong to more than one fleet
        Given my fleet of user "1"
        And the fleet of another user "2"
        And a vehicle with marque "peugeot" and matriculation "AL-ZZ-KKK"
        And this vehicle has been registered into the other user's fleet
        When I register this vehicle into my fleet
        Then this vehicle should be part of my vehicle fleet